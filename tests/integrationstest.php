<?php
require_once 'vendor/autoload.php';

class PagesIntegrationTest extends IntegrationTest{

    public function test_index()
    {
        $response = $this->make_request("GET", "/");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains("velociraptor", $response->getBody()->getContent());
        $this->assertContains("text/html", $response->getHeader('Content-Type')[0]);
    }
}