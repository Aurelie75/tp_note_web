<?php 
function getdinos(){
    $response = Requests::get('https://medusa.delahayeyourself.info/api/dinosaurs/');
    return json_decode($response->body);
}

function getonedino($slug){
    $response = Requests::get('https://medusa.delahayeyourself.info/api/dinosaurs/'.$slug);
    return json_decode($response->body);
}

function troisdinos(){
$colors = array("red","green","blue","yellow","brown");
$random_keys = array_rand($colors, 3); // On récupére trois clés aléatoires
echo $colors[$random_keys[0]];
echo $colors[$random_keys[1]];
echo $colors[$random_keys[2]];
}

function renderHTMLFromMarkdown($string_markdown_formatted)
{
    return trim(Markdown::defaultTransform($string_markdown_formatted));
}

function readFileContent($filepath){
    if(file_exists($filepath)){
        return file_get_contents($filepath);
    }
    throw new Exception("File doesn't exist!");
}


function getPageContent($page_name)
{
    $filepath = sprintf("pages/%s.md", $page_name);
    return readFileContent($filepath);
}