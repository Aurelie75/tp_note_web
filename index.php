<?php require "vendor/autoload.php";

$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
    $twig->addFilter(new Twig_Filter('markdown', function($string){
        return renderHTMLFromMarkdown($string);
    }));
});


Flight::map('render', function($template, $data=array()){
    Flight::view()->display($template, $data);
});

Flight::route('/', function(){
    $data = [
        'dinos' => getdinos(),
    ];
    Flight::render('dino.twig',$data);
});

Flight::route('/dinosaur/{{ dino.slug }}', function(){
  
    Flight::render("https://medusa.delahayeyourself.info/api/dinosaurs/");
});


Flight::route('/dinosaur/@slug', function($slug){
    $data = [
        'dino' => getonedino($slug),
    ];
    Flight::render('detail.twig',$data);
});
Flight::start();
